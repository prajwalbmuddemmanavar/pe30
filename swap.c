#include<stdio.h>
void
swap (int *num1, int *num2)
{
  int temp;
  temp = *num1;
  *num1 = *num2;
  *num2 = temp;
}

int
main ()
{
  int num1, num2;
  printf ("enter two numbers\n");
  scanf ("%d%d", &num1, &num2);
  swap (&num1, &num2);
  printf ("the interchanged numbers are:%d %d", num1, num2);
  return 0;
}