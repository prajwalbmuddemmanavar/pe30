#include<stdio.h>
#include<math.h>
float
input_radius ()
{
  float radius;			/* local to input */
  printf ("enter the value for radius\n");
  scanf ("%f", &radius);
  return radius;
}

float
compute_area (float radius)
{
  M_PI *radius * radius;
  return M_PI * radius * radius;
}

float
compute_circumference (float radius)
{
  2 *M_PI * radius;
  return 2 * M_PI * radius;
}

float
output (float radius, float area)
{
  printf ("the area of the circle with radius:%f is %f\n", radius, area);
}

float
output_circumference (float radius, float circumference)
{
  printf ("the circumference of the circle with radius:%f is %f\n", radius,
	  circumference);
}

int
main ()
{
  float radius, area, circumference;
  radius = input_radius ();
  area = compute_area (radius);
  circumference = compute_circumference (radius);
  output (radius, area);
  output_circumference (radius, circumference);
  return 0;
}